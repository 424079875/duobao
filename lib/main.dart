import 'package:duobao/res/products/product_detail.dart';
import 'package:flutter/material.dart';
import 'res/widget/bottom_navigation.dart';
// import './router.dart';

void main()=>runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter bottomNavigationBar',
      theme: ThemeData.light(),//主题
      home: BottomNavigationWidget(),
      routes: <String, WidgetBuilder> {
        '/productDetail': (BuildContext context) => ProductDetail(),
      },
      // initialRoute: RouteConfig.initRouteName,
      // onGenerateRoute: RouteConfig.onGenerateRoute,
    );
  }
}