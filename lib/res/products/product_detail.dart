import 'package:flutter/material.dart';

class ProductDetail extends StatefulWidget {
  @override
  ProductDetailState createState() => ProductDetailState();
}

class ProductDetailState extends State<ProductDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('商品详情',style: TextStyle(color: Colors.black),),
          // backgroundColor: Colors.white,
          centerTitle: true,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
        ),
        body: Stack(
          children:<Widget>[
            SingleChildScrollView(
            child: Container(
              alignment: Alignment.center,
              decoration: new BoxDecoration(
                color: Color.fromRGBO(242, 242, 242, 1),
              ),
              child: Column(
                    children: [_ProductInfo(),_ProductTabar()],
                  ),
              ) 
            ),
            _ProductDetailBottom()
          ],
        ));
  }
}

class _ProductInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        children: <Widget>[
          Image.network('http://t8.baidu.com/it/u=3571592872,3353494284&fm=79&app=86&size=h300&n=0&g=4n&f=jpeg?sec=1584414126&t=06a80d24fef27d8fa9d4eb085bed0845',width: MediaQuery.of(context).size.width,height: MediaQuery.of(context).size.width,),
          Container(
            padding: EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('小米移动电源2C毫安移动电源/大容量便携超薄支持双向PD快充...',style: TextStyle(color: Colors.black,fontSize: 16),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: [
                        Text(
                          '￥',
                          style: TextStyle(
                              fontSize: 14,
                              color: Color.fromRGBO(255, 61, 44, 1)),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Text('50.00',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color.fromRGBO(255, 61, 44, 1),
                                  fontWeight: FontWeight.w600)),
                        )
                      ]
                    ),
                    Text('期号:【第136期】',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 13),)
                  ],
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child:ClipRRect(
                    child: Stack(
                      children: <Widget>[
                      Positioned(
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 8, 0), 
                          height: 12,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(230, 230, 230, 1),
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          ),
                        )
                      ),
                      Container(
                          width: (MediaQuery.of(context).size.width - 30)*0.8,
                          height: 12,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color.fromRGBO(255, 60, 45, 1),Color.fromRGBO(255, 112, 17, 1)],
                              begin: FractionalOffset(0, 1),
                              end: FractionalOffset(1, 0)
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(12.0))
                          ),
                        ),
                      ]
                    )
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('剩余25867张奖券',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 13),),
                    Text('参与进度:81.6%',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 13),)
                  ],
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Text('声明：本应用所有活动与内容均与苹果公司(Apple inc)无关',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontSize: 12)),
                ),

               ],
            ),
          )
        ],
      ),
    );
  }
}

class _ProductTabar extends StatefulWidget {
  @override
  _ProductTabarState createState() => _ProductTabarState();
}

class _ProductTabarState  extends State<_ProductTabar>{
  int _tabbarindex = 3;
  bool seckill = false;
  List tabbarList;
  initState() {
    super.initState();
      if(seckill){
        tabbarList = [
          {"title": '正在参与','index': 1},
          {"title": '我的兑换','index': 3},
          {"title": '玩法规则','index': 4},
        ];
      }else{
        tabbarList = [
          {"title": '正在参与','index': 1},
          {"title": '我的号码','index': 2},
          {"title": '玩法规则','index': 4},
        ];
      }
  }

  Widget buildTabbar() {
    List<Widget> tabbarWidget = [];
    Widget content;
    for(var item in tabbarList){
      tabbarWidget.add(
        GestureDetector(
          onTap: () {
            setState((){
              print(tabbarWidget);
              _tabbarindex = item['index'];
            });
          },
          child:Container(
            padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
            height: 47,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child:Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              Text(item['title'],
                  style: TextStyle(
                    color: _tabbarindex == item['index'] ? Color.fromRGBO(255, 61, 44, 1) : Color.fromRGBO(51, 51, 51, 1),
                    fontSize: 18,
                  )),
              Container(
                  width: 30,
                  height: 4,
                  decoration: BoxDecoration(
                    //背景
                    gradient: _tabbarindex == item['index'] ? LinearGradient(
                        colors: [
                          Color.fromRGBO(255, 60, 45, 1),
                          Color.fromRGBO(255, 112, 17, 1)
                        ],
                        begin: FractionalOffset(1, 0),
                        end: FractionalOffset(0, 1)): null,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(3),
                        topRight: Radius.circular(3)),
                  ))
            ])
          )
        ),
      );
    }
    content = Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: tabbarWidget
                );
      return content;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 60),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
            height: 47,
            decoration: BoxDecoration(
            color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10)
              ),
            ),
            child: buildTabbar(),
          ),
          Offstage(
            offstage: _tabbarindex != 1,
            child:Container(
              margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
              ),
              child:Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(40),
                      child: Image.network('http://t8.baidu.com/it/u=3571592872,3353494284&fm=79&app=86&size=h300&n=0&g=4n&f=jpeg?sec=1584414126&t=06a80d24fef27d8fa9d4eb085bed0845',width: 40,height: 40,),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text('参与消耗奖券:',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 15),),
                            Text('125张',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 15),),
                          ],
                        ),
                        Text('2019.12.23 12:23',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontSize: 13),)
                      ],
                    )
                  ],
                ),
              )
            )
          ),
          Offstage(
            offstage: _tabbarindex !=2,
            child: Container(
              padding: EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      gradient: LinearGradient(
                        colors: [Color.fromRGBO(255, 60, 45, 0.05),Color.fromRGBO(255, 112, 17, 0.05)],
                        begin: FractionalOffset(0, 0),
                        end: FractionalOffset(1, 0)
                      )
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(12, 0, 9, 0),
                          child: Icon(Icons.error_outline,color: Color.fromRGBO(255, 61, 44, 1),),
                        ),
                        Text('本期获得号码125个，共参与5次',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),fontSize: 15),)
                      ],
                    ),
                  ),
                  Wrap(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        width: (MediaQuery.of(context).size.width - 30) /5,
                        child: Text('282737',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        width: (MediaQuery.of(context).size.width - 30) /5,
                        child: Text('282737',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        width: (MediaQuery.of(context).size.width - 30) /5,
                        child: Text('282737',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        width: (MediaQuery.of(context).size.width - 30) /5,
                        child: Text('282737',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        width: (MediaQuery.of(context).size.width - 30) /5,
                        child: Text('282737',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        width: (MediaQuery.of(context).size.width - 30) /5,
                        child: Text('282737',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        width: (MediaQuery.of(context).size.width - 30) /5,
                        child: Text('282737',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Offstage(
            offstage: _tabbarindex != 3,
            child: Container(
              padding: EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      gradient: LinearGradient(
                        colors: [Color.fromRGBO(255, 60, 45, 0.05),Color.fromRGBO(255, 112, 17, 0.05)],
                        begin: FractionalOffset(0, 0),
                        end: FractionalOffset(1, 0)
                      )
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(12, 0, 9, 0),
                          child: Icon(Icons.error_outline,color: Color.fromRGBO(255, 61, 44, 1),),
                        ),
                        Text('本期获得号码125个，共参与5次',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),fontSize: 15),)
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(0,15,0,15),
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1)))
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('第一次',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 15),),
                            Text('兑换时间：2020.3.12  15:15',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                          ],
                        ),
                      )
                    ],
                  )
                ]
              )
            )
          )
        ],
      )
    );
  }
}

class _ProductDetailBottom extends StatefulWidget {
  _ProductDetailBottomState createState() => _ProductDetailBottomState();
}

class _ProductDetailBottomState  extends State<_ProductDetailBottom>{
    @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: Container(
        height: 50,
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(top: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
        ),
        child: Flex(
          direction: Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row( //上新详情选择按钮
              children: <Widget>[
                Offstage(
                  offstage: false, // 用seckill判断;
                  child: Row(children: <Widget>[
                    Image.asset('assets/images/product_detail_reduce_icon.png',width: 20,height: 20,),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Text('1',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 19,fontWeight: FontWeight.w600),),
                    ),
                    Image.asset('assets/images/product_detail_plus_icon.png',width: 20,height: 20,),
                  ],),
                ),
                Offstage(
                  offstage: true, // 用seckill判断;
                  child: Row(
                    children: <Widget>[
                      Image.asset('assets/images/product_detail_coupon_icon.png',width: 27,height: 23,),
                      Padding(
                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text('1',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 19,fontWeight: FontWeight.w600),),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: Text('(可用156张)',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontSize: 13),),
                  )
                ),
                Container(
                  width: 110,
                  height: 34,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(17),
                    gradient: LinearGradient(
                      colors: [Color.fromRGBO(255, 112, 17, 1),Color.fromRGBO(255, 60, 45, 1)],
                      begin: FractionalOffset(0, 0),
                      end: FractionalOffset(1, 0)
                    )
                  ),
                  child: Text('立即兑换',style: TextStyle(color: Colors.white,fontSize: 15),),
                )
          ],
        )
      ),
    );
  }
}