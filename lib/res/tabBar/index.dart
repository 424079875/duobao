import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('首页',style: TextStyle(color: Colors.black),),
          // backgroundColor: Colors.white,
          centerTitle: true,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              alignment: Alignment.center,
              decoration: new BoxDecoration(
                color: Colors.grey,
                //     image: new DecorationImage(
                //        image: new AssetImage(widget.bgUrl),
                //         //这里是从assets静态文件中获取的，也可以new NetworkImage(）从网络上获取
                //  centerSlice: new Rect.fromLTRB(270.0, 180.0, 1360.0, 730.0),
                //     ),
              ),
              child: Column(
                children: [SwiperPage(), IndexNav(), ProductList()],
              ),
            ),
        )));
  }
}

class SwiperPage extends StatefulWidget {
  @override
  SwiperPageState createState() {
    return SwiperPageState();
  }
}

class SwiperPageState extends State<SwiperPage> {

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 180.0,
        child: Swiper(
          itemBuilder: (BuildContext context, int index) {
            return Image.network(
              "http://via.placeholder.com/288x188",
              fit: BoxFit.fill,
            );
          },
          itemCount: 3,
          autoplay: true,
          pagination: new SwiperPagination(
                builder: DotSwiperPaginationBuilder(
              color: Color.fromRGBO(255, 255, 255, 0.5),
              activeColor: Colors.white,
            ))
        ));
  }
}

class IndexNav extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 345,
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        padding: EdgeInsets.fromLTRB(0, 17, 0, 10),
        // height: 83,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          //背景
          color: Colors.white,
          //设置四周圆角 角度
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                // Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetail()));
                Navigator.pushNamed(context,'/productDetail');
              },
              child:Column(
                children: <Widget>[
                  Image.asset(
                    'assets/images/index_nav_1.png',
                    width: 28,
                  ),
                  Text('视频领券',
                      style: TextStyle(
                      color: Color.fromRGBO(51, 51, 51, 1), height: 2))
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                
              },
              child:Column(
              children: <Widget>[
                Image.asset(
                  'assets/images/index_nav_2.png',
                  width: 30,
                ),
                Text('大转盘',
                    style: TextStyle(
                        color: Color.fromRGBO(51, 51, 51, 1), height: 2))
              ],
            )
            ),
            GestureDetector(
              onTap: () {
                
              },
              child:Column(
              children: <Widget>[
                Image.asset(
                  'assets/images/index_nav_3.png',
                  width: 30,
                ),
                Text('心情分享',
                    style: TextStyle(
                        color: Color.fromRGBO(51, 51, 51, 1), height: 2))
              ],
            )
            ),
            GestureDetector(
              onTap: () {
                
              },
              child:Column(
              children: <Widget>[
                Image.asset(
                  'assets/images/index_nav_4.png',
                  width: 30,
                ),
                Text('晒单',
                    style: TextStyle(
                        color: Color.fromRGBO(51, 51, 51, 1), height: 2))
              ],
            )
            ),
          ],
        ));
  }
}

class ProductList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // 将创建的State返回
    return ProductListState();
  }
}

class ProductListState extends State<ProductList> {
  int _tabbarindex = 1;
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        width: 345,
        decoration: BoxDecoration(
          //背景
          color: Color.fromRGBO(247, 247, 247, 1),
          //设置四周圆角 角度
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
              height: 47,
              decoration: BoxDecoration(
              color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)
                ),
              ),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState((){
                          _tabbarindex = 1;
                        });
                      },
                      child:Container(
                        padding: EdgeInsets.fromLTRB(0, 7, 0, 0),
                        child:Column(children: [
                          Text('每日上新',
                              style: TextStyle(
                                color: _tabbarindex == 1 ? Color.fromRGBO(255, 61, 44, 1) : Color.fromRGBO(51, 51, 51, 1),
                                fontSize: 18,
                              )),
                          Container(
                              width: 30,
                              height: 4,
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                              decoration: BoxDecoration(
                                //背景
                                gradient: _tabbarindex == 1 ? LinearGradient(
                                    colors: [
                                      Color.fromRGBO(255, 60, 45, 1),
                                      Color.fromRGBO(255, 112, 17, 1)
                                    ],
                                    begin: FractionalOffset(1, 0),
                                    end: FractionalOffset(0, 1)): null,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(3),
                                    topRight: Radius.circular(3)),
                              ))
                        ])
                      )
                    ),
                    Container(
                      color: Color.fromRGBO(242, 242, 242, 1),
                      height: 20,
                      width: 1,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState((){
                          _tabbarindex = 2;
                        });
                      },
                      child:Container(
                        padding: EdgeInsets.fromLTRB(0, 7, 0, 0),
                      child:Column(children: [
                      Text('秒杀专区',
                          style: TextStyle(
                            color: _tabbarindex == 2 ? Color.fromRGBO(255, 61, 44, 1) : Color.fromRGBO(51, 51, 51, 1),
                            fontSize: 18,
                          )),
                      Container(
                          width: 30,
                          height: 4,
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          decoration: BoxDecoration(
                            //背景
                            gradient: _tabbarindex == 2 ? LinearGradient(
                                colors: [
                                  Color.fromRGBO(255, 60, 45, 1),
                                  Color.fromRGBO(255, 112, 17, 1)
                                ],
                                begin: FractionalOffset(1, 0),
                                end: FractionalOffset(0, 1)) : null,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(3),
                                topRight: Radius.circular(3)),
                          ))
                    ])
                      )
                    ),
                  ]),
            ),
            Container(
              child: Wrap(
                spacing: 9.0, // 主轴(水平)方向间距
                runSpacing: 10.0, // 纵轴（垂直）方向间距
                alignment: WrapAlignment.spaceBetween, 
                children: <Widget>[
                  Container(
                      width: 168,
                      padding: EdgeInsets.fromLTRB(15, 0, 10, 20),
                      decoration: BoxDecoration(
                      color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                      ),
                      child: Column(
                        children: [
                          Container(
                              padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                              child: Image(
                                image: NetworkImage(
                                    'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
                                height: 120,
                                width: 120,
                              )),
                          Text(
                            '电信/联通/移动充值卡50元话费',
                            style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontSize: 14,
                            ),
                          ),
                          Row(children: [
                            Text(
                              '￥',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Color.fromRGBO(255, 61, 44, 1)),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Text('50.00',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Color.fromRGBO(255, 61, 44, 1),
                                      fontWeight: FontWeight.w600)),
                            )
                          ]),
                          Row(
                            children: <Widget>[
                              ClipRRect(
                                //设置四周圆角 角度
                                borderRadius: BorderRadius.all(Radius.circular(3.0)),
                                child: Stack(
                                  children: <Widget>[
                                  Positioned(
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(0, 0, 8, 0), 
                                      height: 6.0,
                                      width: 114,
                                      decoration: BoxDecoration(
                                        color: Color.fromRGBO(230, 230, 230, 1),
                                        borderRadius: BorderRadius.all(Radius.circular(3.0)),
                                      ),
                                    )
                                  ),
                                  Container(
                                      width: 114*0.8,
                                      height: 6.0,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          colors: [Color.fromRGBO(255, 60, 45, 1),Color.fromRGBO(255, 112, 17, 1)],
                                          begin: FractionalOffset(0, 1),
                                          end: FractionalOffset(1, 0)
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(3.0))
                                      ),
                                    ),
                                  ]
                                )
                              ),
                              Text(
                                '80%',
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(255, 61, 44, 1)),
                              )
                            ],
                          ),
                        ],
                      )),
                  Container(
                      width: 168,
                      padding: EdgeInsets.fromLTRB(15, 0, 10, 20),
                      decoration: BoxDecoration(
                      color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                      ),
                      child: Column(
                        children: [
                          Container(
                              padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                              child: Image(
                                image: NetworkImage(
                                    'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
                                height: 120,
                                width: 120,
                              )),
                          Text(
                            '电信/联通/移动充值卡50元话费',
                            style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontSize: 14,
                            ),
                          ),
                          Row(children: [
                            Text(
                              '￥',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Color.fromRGBO(255, 61, 44, 1)),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Text('50.00',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Color.fromRGBO(255, 61, 44, 1),
                                      fontWeight: FontWeight.w600)),
                            )
                          ]),
                          Row(
                            children: <Widget>[
                              ClipRRect(
                                //设置四周圆角 角度
                                borderRadius: BorderRadius.all(Radius.circular(3.0)),
                                child: Stack(
                                  children: <Widget>[
                                  Positioned(
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(0, 0, 8, 0), 
                                      height: 6.0,
                                      width: 114,
                                      decoration: BoxDecoration(
                                        color: Color.fromRGBO(230, 230, 230, 1),
                                        borderRadius: BorderRadius.all(Radius.circular(3.0)),
                                      ),
                                    )
                                  ),
                                  Container(
                                      width: 114*0.8,
                                      height: 6.0,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          colors: [Color.fromRGBO(255, 60, 45, 1),Color.fromRGBO(255, 112, 17, 1)],
                                          begin: FractionalOffset(0, 1),
                                          end: FractionalOffset(1, 0)
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(3.0))
                                      ),
                                    ),
                                  ]
                                )
                              ),
                              Text(
                                '80%',
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(255, 61, 44, 1)),
                              )
                            ],
                          ),
                        ],
                      )),
                  Container(
                      width: 168,
                      padding: EdgeInsets.fromLTRB(15, 0, 10, 20),
                      decoration: BoxDecoration(
                      color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                      ),
                      child: Column(
                        children: [
                          Container(
                              padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                              child: Image(
                                image: NetworkImage(
                                    'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
                                height: 120,
                                width: 120,
                              )),
                          Text(
                            '电信/联通/移动充值卡50元话费',
                            style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontSize: 14,
                            ),
                          ),
                          Row(children: [
                            Text(
                              '￥',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Color.fromRGBO(255, 61, 44, 1)),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Text('50.00',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Color.fromRGBO(255, 61, 44, 1),
                                      fontWeight: FontWeight.w600)),
                            )
                          ]),
                          Row(
                            children: <Widget>[
                              ClipRRect(
                                //设置四周圆角 角度
                                borderRadius: BorderRadius.all(Radius.circular(3.0)),
                                child: Stack(
                                  children: <Widget>[
                                  Positioned(
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(0, 0, 8, 0), 
                                      height: 6.0,
                                      width: 114,
                                      decoration: BoxDecoration(
                                        color: Color.fromRGBO(230, 230, 230, 1),
                                        borderRadius: BorderRadius.all(Radius.circular(3.0)),
                                      ),
                                    )
                                  ),
                                  Container(
                                      width: 114*0.8,
                                      height: 6.0,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          colors: [Color.fromRGBO(255, 60, 45, 1),Color.fromRGBO(255, 112, 17, 1)],
                                          begin: FractionalOffset(0, 1),
                                          end: FractionalOffset(1, 0)
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(3.0))
                                      ),
                                    ),
                                  ]
                                )
                              ),
                              Text(
                                '80%',
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(255, 61, 44, 1)),
                              )
                            ],
                          ),
                        ],
                      )),
                   ],
              ),
            )
          ],
        ));
  }
}
