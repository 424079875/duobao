import 'package:flutter/material.dart';

class InvitationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            '收徒',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
        ),
        body: Center(
          child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            color: Color.fromRGBO(247, 247, 247, 1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [InvitationHead(),InvitationMain(),InvitationBottom()],
            ),
          ))));
  }
}

class InvitationHead extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        color: Colors.white,
        alignment: Alignment.topCenter,
        
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                decoration:BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/card_bg.png'),fit:BoxFit.fill)),
                child:Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            ClipOval(
                              child:Image.asset(
                                'assets/images/icon_1.png',
                                width: 38,
                                height: 38,
                              )
                            ),
                          Container(
                            margin: EdgeInsets.fromLTRB(12, 0, 0, 0),
                            child:Text('当个好人',style: TextStyle(color: Colors.white,fontSize: 17))
                          )
                        ]),
                        Container(
                          padding: EdgeInsets.fromLTRB(12, 4, 12, 4),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(117, 108, 68, 1),
                            borderRadius: BorderRadius.circular(13)
                          ),
                          child: Text('收益明细 >',style: TextStyle(color: Colors.white,fontSize: 14,),),
                        )
                      ],
                    ),
                    Container(
                        padding: EdgeInsets.fromLTRB(0, 22, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Column(
                             children: <Widget>[
                               Text('2332',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: Colors.white),),
                               Padding(
                                padding: EdgeInsets.fromLTRB(0, 4, 0, 0),
                                child:Text('今日获得券数(张)',style: TextStyle(fontSize: 13,color: Color.fromRGBO(201, 193, 173, 1)),)
                               )
                             ], 
                            ),
                            Container(
                              width: 0.5,
                              height: 33,
                              color: Color.fromRGBO(134, 122, 72, 1),
                            ),
                            Column(
                             children: <Widget>[
                               Text('2332',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: Colors.white),),
                               Padding(
                                padding: EdgeInsets.fromLTRB(0, 4, 0, 0),
                                child:Text('今日获得券数(张)',style: TextStyle(fontSize: 13,color: Color.fromRGBO(201, 193, 173, 1)),)
                               )
                             ], 
                            ),
                          ],
                        ),
                    ),
                  ],
                )
              ),
              Container(
                height: 44,
                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(242, 242, 242, 1),
                  borderRadius: BorderRadius.circular(22)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding:EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child:Row(
                        children: <Widget>[
                          Text('邀请码：',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 16)),
                          Text('shsfd25',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 18)),
                        ], 
                      )
                    ),
                    Container(
                      width: 100,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(255, 60, 45, 1),
                                  Color.fromRGBO(255, 112, 17, 1)
                                ],
                                begin: FractionalOffset(1, 0),
                                end: FractionalOffset(0, 1)
                              ),
                        borderRadius: BorderRadius.all(Radius.circular(22))
                      ),
                      child: Text('点击复制',style: TextStyle(color: Colors.white,fontSize: 15)),
                    ),
                  ],
                ),
              ),
              Container(
                      alignment: Alignment.center,
                      height: 44,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(22)),
                        border: Border.all(color: Color.fromRGBO(255, 112, 17, 1),width: 1)
                      ),
                      child: Text('点击立即邀请好友',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),fontSize: 17,fontWeight: FontWeight.w600),),
                    )
             ],
        ));
  }
}


class InvitationMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 20),
      color: Colors.white,
      alignment: Alignment.topLeft,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('最新收益',style: TextStyle(color: Colors.black,fontWeight: FontWeight.w600,fontSize: 18),),
          Container(
            margin:EdgeInsets.fromLTRB(0, 15, 0, 0),
            alignment: Alignment.center,
            height: 70,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(255, 60, 45, 1),
                                  Color.fromRGBO(255, 112, 17, 1)
                                ],
                                begin: FractionalOffset(1, 0),
                                end: FractionalOffset(0, 1)
                              ),
                        borderRadius: BorderRadius.all(Radius.circular(10))
            ),
              child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Text('2332',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: Colors.white),),
                               Text('今日获得奖券(张)',style: TextStyle(fontSize: 13,color: Color.fromRGBO(255, 255, 255, 0.7)),)
                             ], 
                            ),
                            Container(
                              width: 0.5,
                              height: 33,
                              margin: EdgeInsets.fromLTRB(36, 0, 36, 0),
                              color: Color.fromRGBO(242, 242, 242, 0.5),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Text('2332',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: Colors.white),),
                               Text('今日邀请好友数(人)',style: TextStyle(fontSize: 13,color: Color.fromRGBO(255, 255, 255, 0.7)),)
                             ], 
                            ),
                          ],
                        ),
            )  
        ],
      ),
    );
  }
}


class InvitationBottom extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // 将创建的State返回
    return InvitationBottomState();
  }
}
class InvitationBottomState extends State<InvitationBottom> {
  int _tabbarindex = 1;
    @override

  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
      child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
              decoration: BoxDecoration(
                border: Border.all(color: Color.fromRGBO(242, 242, 242, 1),width: 1),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)
                ),
              ),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState((){
                          _tabbarindex = 1;
                        });
                      },
                      child:Column(
                      children: [
                      Text('邀请好友收益说明',
                          style: TextStyle(
                            color: _tabbarindex == 1 ? Color.fromRGBO(255, 61, 44, 1) : Color.fromRGBO(51, 51, 51, 1),
                            fontSize: 18,
                          )),
                          Container(
                              width: 30,
                              height: 4,
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                              decoration: BoxDecoration(
                                //背景
                                gradient: _tabbarindex == 1 ? LinearGradient(
                                    colors: [
                                      Color.fromRGBO(255, 60, 45, 1),
                                      Color.fromRGBO(255, 112, 17, 1)
                                    ],
                                    begin: FractionalOffset(1, 0),
                                    end: FractionalOffset(0, 1)) : null,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(3),
                                    topRight: Radius.circular(3)),
                            ))
                    ])),
                    GestureDetector(
                      onTap: () {
                        setState((){
                          _tabbarindex = 2;
                        });
                      },
                    child:Column(children: [
                      Text('我的好友',
                          style: TextStyle(
                            color: _tabbarindex == 2 ? Color.fromRGBO(255, 61, 44, 1) : Color.fromRGBO(51, 51, 51, 1),
                            fontSize: 18,
                          )),
                      Container(
                          width: 30,
                          height: 4,
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          decoration: BoxDecoration(
                            //背景
                            gradient: _tabbarindex == 2 ? LinearGradient(
                                colors: [
                                  Color.fromRGBO(255, 60, 45, 1),
                                  Color.fromRGBO(255, 112, 17, 1)
                                ],
                                begin: FractionalOffset(1, 0),
                                end: FractionalOffset(0, 1)):null,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(3),
                                topRight: Radius.circular(3)),
                          ))
                    ])),
                  ]),
            ),
            Offstage(
              offstage: _tabbarindex == 1 ? false : true,
              child: Column(
                children: <Widget>[
                  Container(
              padding: EdgeInsets.fromLTRB(15, 20, 15, 15),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      width: 105,
                      height: 80,
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 243, 242, 1),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Color.fromRGBO(255, 91, 76, 1),width: 1)
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('获得一级好友享X张券',style: TextStyle(color: Color.fromRGBO(255, 79, 35, 1),fontSize: 14,),textAlign:TextAlign.center),
                          Container(
                            width: 80,
                            height: 20,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(255, 60, 45, 1),
                                  Color.fromRGBO(255, 112, 17, 1)
                                ],
                                begin: FractionalOffset(1, 0),
                                end: FractionalOffset(0, 1)
                              ),
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(9),topRight: Radius.circular(9))
                            ),
                            child: Text('我邀请的',style: TextStyle(color: Colors.white,fontSize: 13),textAlign: TextAlign.center,),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 105,
                      height: 80,
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(242, 249, 255, 1),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Color.fromRGBO(48, 155, 242, 1),width: 1)
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('获得二级好友享X张券',style: TextStyle(color: Color.fromRGBO(11, 137, 241, 1),fontSize: 14,),textAlign:TextAlign.center),
                          Container(
                            width: 80,
                            height: 20,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(0, 129, 239, 1),
                                  Color.fromRGBO(55, 165, 248, 1)
                                ],
                                begin: FractionalOffset(1, 0),
                                end: FractionalOffset(0, 1)
                              ),
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(9),topRight: Radius.circular(9))
                            ),
                            child: Text('一级邀请的',style: TextStyle(color: Colors.white,fontSize: 13),textAlign: TextAlign.center,),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 105,
                      height: 80,
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(251, 242, 255, 1),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Color.fromRGBO(190, 51, 255, 1),width: 1)
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('获得三级级好友享X张券',style: TextStyle(color: Color.fromRGBO(178, 26, 255, 1),fontSize: 14,),textAlign:TextAlign.center),
                          Container(
                            width: 80,
                            height: 20,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(149, 46, 255, 1),
                                  Color.fromRGBO(182, 25, 255, 1)
                                ],
                                begin: FractionalOffset(1, 0),
                                end: FractionalOffset(0, 1)
                              ),
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(9),topRight: Radius.circular(9))
                            ),
                            child: Text('二级邀请的',style: TextStyle(color: Colors.white,fontSize: 13),textAlign: TextAlign.center,),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 5,
                          height: 5,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 169, 145, 1),
                            borderRadius: BorderRadius.all(Radius.circular(5))
                          ),
                        ),
                        Container(
                          width: 7,
                          height: 7,
                          margin: EdgeInsets.fromLTRB(7, 0, 15, 0),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 82, 32, 1),
                            borderRadius: BorderRadius.all(Radius.circular(7))
                          ),
                        ),
                        Text('如何获得高收益',style: TextStyle(color: Colors.black,fontSize: 17,fontWeight: FontWeight.w700),),
                        Container(
                          width: 7,
                          height: 7,
                          margin: EdgeInsets.fromLTRB(15, 0, 7, 0),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 82, 32, 1),
                            borderRadius: BorderRadius.all(Radius.circular(7))
                          ),
                        ),
                        Container(
                          width: 5,
                          height: 5,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 169, 145, 1),
                            borderRadius: BorderRadius.all(Radius.circular(5))
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 8,
                              height: 8,
                              margin: EdgeInsets.fromLTRB(0, 8, 10, 0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(color: Color.fromRGBO(255, 61, 44, 1),width: 1.5)
                              ),
                            ),
                            Text('邀请身边朋友、亲戚、同时更容易“收徒成功”。',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 15),)
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 8,
                              height: 8,
                              margin: EdgeInsets.fromLTRB(0, 8, 10, 0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(color: Color.fromRGBO(255, 61, 44, 1),width: 1.5)
                              ),
                            ),
                            Expanded(
                              child:Text('充分发挥微信、微博、QQ、贴吧等社交软件，有效利用资源，收徒更迅速。”。',maxLines:2,overflow:TextOverflow.visible,style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 15),)
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 8,
                              height: 8,
                              margin: EdgeInsets.fromLTRB(0, 8, 10, 0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(color: Color.fromRGBO(255, 61, 44, 1),width: 1.5)
                              ),
                            ),
                            Expanded(
                              child:Text('动员一级、二级好友多去收徒，三重收益更丰。',maxLines:2,overflow:TextOverflow.visible,style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 15),)
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  
                ],
              ),
            ),
            Offstage(
              offstage: _tabbarindex == 2 ? false : true,
              child:Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child:Column(
                children:<Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 15, 0, 15), 
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
                    ),
                      child:Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ClipOval(
                            child:Image.network(
                              'https://tpc.googlesyndication.com/simgad/2267810362956640009?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0DwkwlYAWBfcAKAAQGIAQGdAQAAgD-oAQGwAYCt4gS4AV_FAS2ynT4&rs=AOga4qn7mAcd2fT5GMJ4CtJAM2cMRU4bpg',
                              width: 60,
                              height: 60,
                            )
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text('少年郎',style: TextStyle(color: Colors.black,fontSize: 17),),
                                      Container(
                                        height: 16,
                                        width: 33,
                                        margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            colors: [
                                              Color.fromRGBO(255, 60, 45, 1),
                                              Color.fromRGBO(255, 112, 17, 1)
                                            ],
                                            begin: FractionalOffset(1, 0),
                                            end: FractionalOffset(0, 1)
                                          ),
                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(6),bottomRight: Radius.circular(6))
                                        ),
                                        child: Text('1级',style: TextStyle(color: Colors.white,fontSize: 12,height: 1),textAlign: TextAlign.center,),
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding:EdgeInsets.fromLTRB(0, 2, 0, 0),
                                    child:Text('2019.12.29  16:12',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontSize: 13),)
                                  ),
                                ],
                              ),
                            )
                          ),
                          Text('我邀请的',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 13),)
                        ],
                      ),
                    ),
                        Container(
                    padding: EdgeInsets.fromLTRB(0, 15, 0, 15), 
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
                    ),
                      child:Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ClipOval(
                            child:Image.network(
                              'https://tpc.googlesyndication.com/simgad/2267810362956640009?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0DwkwlYAWBfcAKAAQGIAQGdAQAAgD-oAQGwAYCt4gS4AV_FAS2ynT4&rs=AOga4qn7mAcd2fT5GMJ4CtJAM2cMRU4bpg',
                              width: 60,
                              height: 60,
                            )
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text('少年郎',style: TextStyle(color: Colors.black,fontSize: 17),),
                                      Container(
                                        height: 16,
                                        width: 33,
                                        margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            colors: [
                                              Color.fromRGBO(255, 60, 45, 1),
                                              Color.fromRGBO(255, 112, 17, 1)
                                            ],
                                            begin: FractionalOffset(1, 0),
                                            end: FractionalOffset(0, 1)
                                          ),
                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(6),bottomRight: Radius.circular(6))
                                        ),
                                        child: Text('1级',style: TextStyle(color: Colors.white,fontSize: 12,height: 1),textAlign: TextAlign.center,),
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding:EdgeInsets.fromLTRB(0, 2, 0, 0),
                                    child:Text('2019.12.29  16:12',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontSize: 13),)
                                  ),
                                ],
                              ),
                            )
                          ),
                          Text('我邀请的',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 13),)
                        ],
                      ),
                    ),
                        Container(
                    padding: EdgeInsets.fromLTRB(0, 15, 0, 15), 
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
                    ),
                      child:Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ClipOval(
                            child:Image.network(
                              'https://tpc.googlesyndication.com/simgad/2267810362956640009?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0DwkwlYAWBfcAKAAQGIAQGdAQAAgD-oAQGwAYCt4gS4AV_FAS2ynT4&rs=AOga4qn7mAcd2fT5GMJ4CtJAM2cMRU4bpg',
                              width: 60,
                              height: 60,
                            )
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text('少年郎',style: TextStyle(color: Colors.black,fontSize: 17),),
                                      Container(
                                        height: 16,
                                        width: 33,
                                        margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            colors: [
                                              Color.fromRGBO(255, 60, 45, 1),
                                              Color.fromRGBO(255, 112, 17, 1)
                                            ],
                                            begin: FractionalOffset(1, 0),
                                            end: FractionalOffset(0, 1)
                                          ),
                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(6),bottomRight: Radius.circular(6))
                                        ),
                                        child: Text('1级',style: TextStyle(color: Colors.white,fontSize: 12,height: 1),textAlign: TextAlign.center,),
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding:EdgeInsets.fromLTRB(0, 2, 0, 0),
                                    child:Text('2019.12.29  16:12',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontSize: 13),)
                                  ),
                                ],
                              ),
                            )
                          ),
                          Text('我邀请的',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 13),)
                        ],
                      ),
                    )
                ]
              )
            )
            )
          ],
      )
    );
  }
}