import 'package:flutter/material.dart';

class MineScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            '个人中心',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
        ),
        body: Center(
          child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.topCenter,
            color: Color.fromRGBO(247, 247, 247, 1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [MineHead(),MineMain(),MineBottom()],
            ),
          ))));
  }
}

class MineHead extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // 将创建的State返回
    return MineHeadState();
  }
}

class MineMain extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // 将创建的State返回
    return MineMainState();
  }
}

class MineBottom extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // 将创建的State返回
    return MineBottomState();
  }
}

class MineHeadState extends State<MineHead> {
    @override

  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 110,  
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors:[Color.fromRGBO(255, 112, 17, 1),Color.fromRGBO(255, 85, 45, 1)],
                begin: FractionalOffset(0,0),
                end: FractionalOffset(1,0)
              )
            ),
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(15, 0, 10, 0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(255, 155, 96, 1),width: 4),
                    borderRadius: BorderRadius.circular(70)
                  ),
                 child: ClipOval(
                    child:Image.network(
                      'https://tpc.googlesyndication.com/simgad/2267810362956640009?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0DwkwlYAWBfcAKAAQGIAQGdAQAAgD-oAQGwAYCt4gS4AV_FAS2ynT4&rs=AOga4qn7mAcd2fT5GMJ4CtJAM2cMRU4bpg',
                      width: 70,
                      height: 70,
                    )
                  ), 
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('一天一天',style: TextStyle(color: Colors.white,fontSize: 19,fontWeight: FontWeight.w500),textAlign: TextAlign.start,),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                        child:Text('ID:256365',style: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.5),fontSize: 14),textAlign: TextAlign.start),
                      )
                    ],
                  )
                ),
                RaisedButton(
                  onPressed: (){

                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(30), bottomLeft: Radius.circular(30))
                  ),
                  child:Row(
                    children: <Widget>[
                      Icon(Icons.power_settings_new),
                      Padding(
                        padding: EdgeInsets.fromLTRB(2, 0, 0, 0),
                        child:Text("退出登录")
                      )
                    ],
                  ),
                  color: Color.fromRGBO(255, 126, 81, 1),
                  textColor: Colors.white,
                  splashColor: Color.fromRGBO(255, 255, 255, 0.5),
                
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: Text('我的订单',style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w600)),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 25),
            child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          width: 30,
                          height: 40,
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/images/mine_nav_1.png',
                            width: 25,
                          ),
                        ),
                        Text('全部订单',
                            style: TextStyle(
                                color: Color.fromRGBO(51, 51, 51, 1)))
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          width: 30,
                          height: 40,
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/images/mine_nav_2.png',
                            width: 30,
                          ),
                        ),
                        Text('待发货',
                            style: TextStyle(
                                color: Color.fromRGBO(51, 51, 51, 1)))
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          width: 30,
                          height: 40,
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/images/mine_nav_3.png',
                            width: 29,
                          ),
                        ),
                        Text('待收货',
                            style: TextStyle(
                                color: Color.fromRGBO(51, 51, 51, 1)))
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          width: 30,
                          height: 40,
                          alignment: Alignment.center,
                          child: Image.asset(
                            'assets/images/mine_nav_4.png',
                            width: 25,
                          ),
                        ),
                        Text('已完成',
                            style: TextStyle(
                                color: Color.fromRGBO(51, 51, 51, 1)))
                      ],
                    ),
                  ],
                )
        ,)],
      ),
    );
    }
}

class MineMainState extends State<MineMain> {
    @override

  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Container(
            padding:EdgeInsets.fromLTRB(0,0,0,15),
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('我的资产',style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w600),),
                Row(
                  children: <Widget>[
                    Text('收益明细',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14,height: 1.25),),
                    Icon(Icons.keyboard_arrow_right,color: Color.fromRGBO(204, 204, 204, 1),)
                  ],
                )
              ],
            ),
          ),
          Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Column(
                             children: <Widget>[
                               Text('2332',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: Colors.black),),
                               Padding(
                                padding: EdgeInsets.fromLTRB(0, 4, 0, 0),
                                child:Text('总奖券(张)',style: TextStyle(fontSize: 13,color: Color.fromRGBO(153, 153, 153, 1)),)
                               )
                             ], 
                            ),
                            Container(
                              width: 0.5,
                              height: 33,
                              color: Color.fromRGBO(242, 242, 242, 1),
                              margin: EdgeInsets.fromLTRB(56, 0, 50, 0),
                            ),
                            Column(
                             children: <Widget>[
                               Text('2332',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: Colors.black),),
                               Padding(
                                padding: EdgeInsets.fromLTRB(0, 4, 0, 0),
                                child:Text('可用奖券(张)',style: TextStyle(fontSize: 13,color: Color.fromRGBO(153, 153, 153, 1)),)
                               )
                             ], 
                            ),
                          ],
                        ),
                    ),
        ],
      ),
    );
  }
}


class MineBottomState extends State<MineBottom> {
    @override

  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(15, 0, 5, 0),
      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        children: <Widget>[
          Container(
            height: 55,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 55,
                      margin: EdgeInsets.fromLTRB(0, 0, 9, 0),
                      alignment: Alignment.center,
                      child:Image.asset('assets/images/mine_bottom_nav_1.png')
                    ),
                    Text('中奖记录',style: TextStyle(color: Colors.black,fontSize: 16,height: 1),)
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text('查看中奖记录',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14),),
                    Icon(Icons.keyboard_arrow_right,color: Color.fromRGBO(153, 153, 153, 1))
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 55,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 55,
                      margin: EdgeInsets.fromLTRB(0, 0, 9, 0),
                      alignment: Alignment.center,
                      child:Image.asset('assets/images/mine_bottom_nav_2.png')
                    ),
                    Text('收货地址',style: TextStyle(color: Colors.black,fontSize: 16,height: 1),)
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text('新增或修改收获地址',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14,height: 1),),
                    Icon(Icons.keyboard_arrow_right,color: Color.fromRGBO(153, 153, 153, 1),)
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 55,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 55,
                      margin: EdgeInsets.fromLTRB(0, 0, 9, 0),
                      alignment: Alignment.center,
                      child:Image.asset('assets/images/mine_bottom_nav_3.png')
                    ),
                    Text('我的邀请人',style: TextStyle(color: Colors.black,fontSize: 16,height: 1),)
                  ],
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  child:Text('小明',style: TextStyle(color: Colors.black,fontSize: 14),)
                )
              ],
            ),
          ),
          Container(
            height: 55,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 55,
                      margin: EdgeInsets.fromLTRB(0, 0, 9, 0),
                      alignment: Alignment.center,
                      child:Image.asset('assets/images/mine_bottom_nav_4.png',width: 16,)
                    ),
                    Text('绑定手机',style: TextStyle(color: Colors.black,fontSize: 16,height: 1),)
                  ],
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  child:Text('183****5632',style: TextStyle(color: Colors.black,fontSize: 14),)
                )
              ],
            ),
          ),
          Container(
            height: 55,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 55,
                      margin: EdgeInsets.fromLTRB(0, 0, 9, 0),
                      alignment: Alignment.center,
                      child:Image.asset('assets/images/mine_bottom_nav_5.png')
                    ),
                    Text('帮助中心',style: TextStyle(color: Colors.black,fontSize: 16,height: 1),)
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text('常见问题解决办法',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14,height: 1),),
                    Icon(Icons.keyboard_arrow_right,color: Color.fromRGBO(153, 153, 153, 1),)
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 55,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 55,
                      margin: EdgeInsets.fromLTRB(0, 0, 9, 0),
                      alignment: Alignment.center,
                      child:Image.asset('assets/images/mine_bottom_nav_6.png')
                    ),
                    Text('关于我们',style: TextStyle(color: Colors.black,fontSize: 16,height: 1),)
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text('APP介绍',style: TextStyle(color: Color.fromRGBO(102, 102, 102, 1),fontSize: 14,height: 1),),
                    Icon(Icons.keyboard_arrow_right,color: Color.fromRGBO(153, 153, 153, 1),)
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 55,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(242, 242, 242, 1),width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 55,
                      margin: EdgeInsets.fromLTRB(0, 0, 9, 0),
                      alignment: Alignment.center,
                      child:Image.asset('assets/images/mine_bottom_nav_7.png')
                    ),
                    Text('联系客服',style: TextStyle(color: Colors.black,fontSize: 16,height: 1),)
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}