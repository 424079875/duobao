import 'package:flutter/material.dart';
import './index.dart';
import './task.dart';
import './invitation.dart';
import './mine.dart';
 
class BottomNavigationWidget extends StatefulWidget {
 
 @override
 _BottomNavigationWidgetState createState() => new _BottomNavigationWidgetState();
}
 
class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {
 final List<Widget> list = List();
 int _currentIndex = 0;
 @override
 void initState() {
  list
   ..add(HomeScreen())
   ..add(TaskScreen())
   ..add(InvitationScreen())
   ..add(MineScreen());
  super.initState();
 }
 
 @override
 Widget build(BuildContext context) {
  return Scaffold(
   body: list[_currentIndex],
   bottomNavigationBar: BottomNavigationBar(
    currentIndex: _currentIndex,
     onTap: (int index){
      setState(() {
       _currentIndex = index;
      });
     },
     type: BottomNavigationBarType.fixed,
     items: [
      BottomNavigationBarItem(
        // icon: Icon(Icons.home),
       icon: Image.asset(
         'assets/images/tabbar/icon_1.png',
          width: 24
       ),
       activeIcon: Image.asset('assets/images/tabbar/icon_1_active.png',width: 24 ),
       title: Text('首页')
      ),
      BottomNavigationBarItem(
        icon: Image.asset('assets/images/tabbar/icon_2.png',width: 24),
        activeIcon: Image.asset('assets/images/tabbar/icon_2_active.png',width: 24),
        title: Text('任务乐园')
      ),
      BottomNavigationBarItem(
        icon: Image.asset('assets/images/tabbar/icon_3.png',width: 24),
        activeIcon: Image.asset('assets/images/tabbar/icon_3_active.png',width: 24),
        title: Text('收徒')
      ),
      BottomNavigationBarItem(
        icon: Image.asset('assets/images/tabbar/icon_4.png',width: 24),
        activeIcon: Image.asset('assets/images/tabbar/icon_4_active.png',width: 24),
        title: Text('个人中心')
      ),
     ],
     iconSize: 12.0,
     selectedItemColor: Color.fromRGBO(255, 61, 44, 1),
     unselectedItemColor: Color.fromRGBO(77, 77, 77, 1),
   ),
  );
 }
}