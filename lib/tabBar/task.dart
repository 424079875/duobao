import 'package:flutter/material.dart';

class TaskScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            '任务乐园',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
        ),
        body: Center(
            child: Center(
                child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            color: Color.fromRGBO(247, 247, 247, 1),
            child: Column(
              children: [TaskHead(), TaskSign(), TaskList()],
            ),
          ),
        ))));
  }
}

class TaskHead extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        color: Colors.white,
        alignment: Alignment.center,
        
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                decoration:BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/card_bg.png'),fit:BoxFit.fill)),
                child:Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            ClipOval(
                              child:Image.asset(
                                'assets/images/icon_1.png',
                                width: 38,
                                height: 38,
                              )
                            ),
                          Container(
                            margin: EdgeInsets.fromLTRB(12, 0, 0, 0),
                            child:Text('当个好人',style: TextStyle(color: Colors.white,fontSize: 17))
                          )
                        ]),
                        Container(
                          padding: EdgeInsets.fromLTRB(12, 4, 12, 4),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(117, 108, 68, 1),
                            borderRadius: BorderRadius.circular(13)
                          ),
                          child: Text('收益明细 >',style: TextStyle(color: Colors.white,fontSize: 14,),),
                        )
                      ],
                    ),
                    Container(
                        padding: EdgeInsets.fromLTRB(0, 22, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Column(
                             children: <Widget>[
                               Text('2332',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: Colors.white),),
                               Padding(
                                padding: EdgeInsets.fromLTRB(0, 4, 0, 0),
                                child:Text('今日获得券数(张)',style: TextStyle(fontSize: 13,color: Color.fromRGBO(201, 193, 173, 1)),)
                               )
                             ], 
                            ),
                            Container(
                              width: 0.5,
                              height: 33,
                              color: Color.fromRGBO(134, 122, 72, 1),
                            ),
                            Column(
                             children: <Widget>[
                               Text('2332',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: Colors.white),),
                               Padding(
                                padding: EdgeInsets.fromLTRB(0, 4, 0, 0),
                                child:Text('今日获得券数(张)',style: TextStyle(fontSize: 13,color: Color.fromRGBO(201, 193, 173, 1)),)
                               )
                             ], 
                            ),
                          ],
                        ),
                    ),
                  ],
                )
              ),
              Image.asset('assets/images/task_advert.png'),
              Padding(padding: EdgeInsets.fromLTRB(0, 12, 0, 0),
              child:   Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Image.asset(
                          'assets/images/task_nav_1.png',
                          width: 30,
                        ),
                        Text('摇摇乐',
                            style: TextStyle(
                                color: Color.fromRGBO(51, 51, 51, 1), height: 2))
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Image.asset(
                          'assets/images/task_nav_2.png',
                          width: 30,
                        ),
                        Text('社交领券',
                            style: TextStyle(
                                color: Color.fromRGBO(51, 51, 51, 1), height: 2))
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Image.asset(
                          'assets/images/task_nav_3.png',
                          width: 30,
                        ),
                        Text('倒计时红包',
                            style: TextStyle(
                                color: Color.fromRGBO(51, 51, 51, 1), height: 2))
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Image.asset(
                          'assets/images/task_nav_4.png',
                          width: 26,
                        ),
                        Text('整点红包',
                            style: TextStyle(
                                color: Color.fromRGBO(51, 51, 51, 1), height: 2))
                      ],
                    ),
                  ],
                )
            )
             ],
        ));
  }
}

class TaskSign extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      padding: EdgeInsets.all(15),
      color: Colors.white,
      child: Column(
       children: <Widget>[
         Row(
          children: <Widget>[
            Text('每日签到',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.w700)),
            Padding(padding: EdgeInsets.fromLTRB(10, 0, 10, 0),child: Text('(签到领取抽奖券)',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontSize: 13)),)
          ], 
         ),
         Stack(
          children: <Widget>[
            Positioned(
              child: Container(
                margin: EdgeInsets.fromLTRB(15,30,15,0),
                height: 1,
                color: Color.fromRGBO(242, 242, 242, 1)
              )
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5), 
                      width: 27,
                      height: 23,
                      child: Image.asset('assets/images/sign_prize_5.png',fit:BoxFit.fill,),
                      ),
                      Text('1天',style: TextStyle(fontSize: 12,color: Color.fromRGBO(153, 153, 153, 1)),)
                    ], 
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5), 
                      width: 27,
                      height: 23,
                      child: Image.asset('assets/images/sign_prize_5.png',fit:BoxFit.fill,),
                      ),
                      Text('2天',style: TextStyle(fontSize: 12,color: Color.fromRGBO(153, 153, 153, 1)),)
                    ], 
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5), 
                      width: 27,
                      height: 23,
                      child: Image.asset('assets/images/sign_prize_10.png',fit:BoxFit.fill,),
                      ),
                      Text('3天',style: TextStyle(fontSize: 12,color: Color.fromRGBO(153, 153, 153, 1)),)
                    ], 
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5), 
                      width: 27,
                      height: 23,
                      child: Image.asset('assets/images/sign_prize_5.png',fit:BoxFit.fill,),
                      ),
                      Text('4天',style: TextStyle(fontSize: 12,color: Color.fromRGBO(153, 153, 153, 1)),)
                    ], 
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5), 
                      width: 27,
                      height: 23,
                      child: Image.asset('assets/images/sign_prize_5.png',fit:BoxFit.fill,),
                      ),
                      Text('5天',style: TextStyle(fontSize: 12,color: Color.fromRGBO(153, 153, 153, 1)),)
                    ], 
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5), 
                      width: 27,
                      height: 23,
                      child: Image.asset('assets/images/sign_prize_5.png',fit:BoxFit.fill,),
                      ),
                      Text('6天',style: TextStyle(fontSize: 12,color: Color.fromRGBO(153, 153, 153, 1)),)
                    ], 
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5), 
                      width: 27,
                      height: 23,
                      child: Image.asset('assets/images/sign_grand_prize.png',fit:BoxFit.fill,),
                      ),
                      Text('7天',style: TextStyle(fontSize: 12,color: Color.fromRGBO(153, 153, 153, 1)),)
                    ], 
                  ),
                ], 
              )
            )   
          ],
        ),
         Container(
            margin: EdgeInsets.fromLTRB(15, 0, 15, 12),
            height: 44,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(22),
              gradient: LinearGradient(
                begin: Alignment(0, -1),
                end: Alignment(0, 1),
                  colors: [
                    Color.fromRGBO(255, 112, 17, 1),
                    Color.fromRGBO(255, 60, 45, 1)
                  ]
              )
            ),
            child: Text('签到领取抽奖券',style: TextStyle(color: Colors.white,fontSize: 17),),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('连续签到',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 13),),
              Text('7天',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),fontSize: 13),),
              Text('，即可领取',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 13),),
              Text('20张',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),fontSize: 13),),
              Text('抽奖券',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 13),),

           ], 
          )
       ], 
      ),
    );
  }
}

class TaskList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
       children: <Widget>[
         Padding(
           padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
           child:Row(
            children: <Widget>[
              Text('每日任务',style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.w700)),
              Padding(padding: EdgeInsets.fromLTRB(10, 0, 10, 0),child: Text('(签到领取抽奖券)',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontSize: 13)),)
            ], 
           ),
         ),
         Column(
           children: <Widget>[
             Container(
               padding:EdgeInsets.fromLTRB(0, 15, 0, 15),
               decoration: BoxDecoration(
                 border: Border(bottom: BorderSide(width: 1,color: Color.fromRGBO(242, 242, 242, 1)))
               ),
               child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('完成首次心情分享：0/1',style:TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16)),
                      Padding(padding: EdgeInsets.fromLTRB(0, 4, 0, 0),child: Text('赠送5张抽奖券',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1)),),)
                    ],
                  ),
                  Container(
                    width: 70,
                    height: 25,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color.fromRGBO(255, 112, 17, 1)),
                      borderRadius: BorderRadius.circular(12)
                    ),
                    child: Text('去完成',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),),),
                  )
                ],
              )
             ),
             Container(
               padding:EdgeInsets.fromLTRB(0, 15, 0, 15),
               decoration: BoxDecoration(
                 border: Border(bottom: BorderSide(width: 1,color: Color.fromRGBO(242, 242, 242, 1)))
               ),
               child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('完成首次心情分享：0/1',style:TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16)),
                      Padding(padding: EdgeInsets.fromLTRB(0, 4, 0, 0),child: Text('赠送5张抽奖券',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1)),),)
                    ],
                  ),
                  Container(
                    width: 70,
                    height: 25,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color.fromRGBO(255, 112, 17, 1)),
                      borderRadius: BorderRadius.circular(12)
                    ),
                    child: Text('去完成',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),),),
                  )
                ],
              )
             ),
             Container(
               padding:EdgeInsets.fromLTRB(0, 15, 0, 15),
               decoration: BoxDecoration(
                 border: Border(bottom: BorderSide(width: 1,color: Color.fromRGBO(242, 242, 242, 1)))
               ),
               child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('完成首次心情分享：0/1',style:TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16)),
                      Padding(padding: EdgeInsets.fromLTRB(0, 4, 0, 0),child: Text('赠送5张抽奖券',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1)),),)
                    ],
                  ),
                  Container(
                    width: 70,
                    height: 25,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color.fromRGBO(255, 112, 17, 1)),
                      borderRadius: BorderRadius.circular(12)
                    ),
                    child: Text('去完成',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),),),
                  )
                ],
              )
             ),
             Container(
               padding:EdgeInsets.fromLTRB(0, 15, 0, 15),
               decoration: BoxDecoration(
                 border: Border(bottom: BorderSide(width: 1,color: Color.fromRGBO(242, 242, 242, 1)))
               ),
               child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('完成首次心情分享：0/1',style:TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16)),
                      Padding(padding: EdgeInsets.fromLTRB(0, 4, 0, 0),child: Text('赠送5张抽奖券',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1)),),)
                    ],
                  ),
                  Container(
                    width: 70,
                    height: 25,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color.fromRGBO(255, 112, 17, 1)),
                      borderRadius: BorderRadius.circular(12)
                    ),
                    child: Text('去完成',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),),),
                  )
                ],
              )
             ),
             Container(
               padding:EdgeInsets.fromLTRB(0, 15, 0, 15),
               decoration: BoxDecoration(
                 border: Border(bottom: BorderSide(width: 1,color: Color.fromRGBO(242, 242, 242, 1)))
               ),
               child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('完成首次心情分享：0/1',style:TextStyle(color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16)),
                      Padding(padding: EdgeInsets.fromLTRB(0, 4, 0, 0),child: Text('赠送5张抽奖券',style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1)),),)
                    ],
                  ),
                  Container(
                    width: 70,
                    height: 25,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color.fromRGBO(255, 112, 17, 1)),
                      borderRadius: BorderRadius.circular(12)
                    ),
                    child: Text('去完成',style: TextStyle(color: Color.fromRGBO(255, 61, 44, 1),),),
                  )
                ],
              )
             ),
             
           ],
         ),
       ], 
      ),
    );
  }
}
